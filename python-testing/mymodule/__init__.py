from .palindrome import is_palindrome
from .data_generation import generate_data
from .histogramming import histogram
from .plotting import plot_histogram